package com.ljupka.shoppinglist.shoppinglist;

import lombok.Value;

import java.util.List;

/**
 * Created by ljupka on 4/1/20.
 */
@Value
public class ItemList {
    List<Item> itemList;
}
