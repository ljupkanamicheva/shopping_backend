package com.ljupka.shoppinglist.shoppinglist;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

@RestController
public class ListsResource {

    @GetMapping("/api/lists/by-user/{userId}")
    public ResponseEntity<ItemList> getPlayerById(@PathVariable String userId){
        List<Item> items = new LinkedList<>();
        items.add(new Item("Gz Papir"));
        items.add(new Item("Jajca"));
        items.add(new Item("Zejtin"));
        ItemList itemList = new ItemList(items);
        return ResponseEntity.ok(itemList);
    }

    @GetMapping("/api/lists/test")
    public ResponseEntity<String> daBE(){
        return ResponseEntity.ok("vleze");
    }
}
