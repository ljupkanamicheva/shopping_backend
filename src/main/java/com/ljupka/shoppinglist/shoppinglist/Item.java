package com.ljupka.shoppinglist.shoppinglist;

import lombok.RequiredArgsConstructor;
import lombok.Value;


/**
 * Created by Ljupka on 4/1/20.
 */

@Value
@RequiredArgsConstructor
public class Item {
    private String name;
}
